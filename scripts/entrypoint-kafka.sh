#!/bin/sh

DIR=$(dirname "${0}")

# For Kafka properties, see
# https://kafka.apache.org/documentation/#configuration

if [ -n "${BROKER_ID}" ]; then
	export PROP_KAFKA_broker_id="${BROKER_ID}"
elif [ -z "${PROP_KAFKA_broker_id}" ]; then
	# a unique broker id will be generated staringt from reserved.broker.max.id + 1
	[ -z "${PROP_KAFKA_broker_id_generation_enable}" ] && export PROP_KAFKA_broker_id_generation_enable=true
fi

if [ -n "${PROP_KAFKA_log_dirs}" ]; then
	export LOG_DIRS="${PROP_KAFKA_log_dirs}"
elif [ -n "${PROP_KAFKA_log_dir}" ]; then
	export LOG_DIRS="${PROP_KAFKA_log_dir}"
fi
if [ -n "${LOG_DIRS}" ]; then
	for LOG_DIR in ${LOG_DIRS//,/ }; do
		mkdir -vp "${LOG_DIR}"
		chmod -v 700 "${LOG_DIR}"
		chown -R kafka:kafka "${LOG_DIR}"
	done
	export PROP_KAFKA_log_dirs="${LOG_DIRS}"
fi

if [ -n "${ZOO_CONNECT}" ]; then
	export PROP_KAFKA_zookeeper_connect="${ZOO_CONNECT}"
elif [ -z "${PROP_KAFKA_zookeeper_connect}" ]; then
	echo "Variable 'ZOO_CONNECT' has to be set correctly to one or more Zookeeper host strings (the comma separated host:port pairs)!" >&2
	exit 1
fi

if [ -n "${LISTENERS}" ]; then
	export PROP_KAFKA_listeners="${LISTENERS}"
elif [ -z "${PROP_KAFKA_listeners}" ]; then
	export PROP_KAFKA_listeners="PLAINTEXT://:9092"
	# with a SSL configuration
	#export PROP_KAFKA_broker_id="PLAINTEXT://:9092,SSL://:9093"
fi

. "${DIR}/kafka-set-props.sh"

exec su kafka -c "exec ${KAFKA_HOME}/bin/kafka-server-start.sh ${KAFKA_SERVER_CONF} ${@}"
