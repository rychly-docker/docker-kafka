#!/bin/sh

DIR=$(dirname "${0}")

if [ "${ROLE:=kafka}" != "none" ]; then
	. "${DIR}/entrypoint-${ROLE}.sh" ${@}
else
	. "${DIR}/kafka-set-props.sh"
	exec ${@}
fi
