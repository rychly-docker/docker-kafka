# Apache Kafka Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-kafka/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-kafka/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-kafka/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-kafka/commits/master)

The image is based on [adoptopenjdk/openjdk8](https://hub.docker.com/r/adoptopenjdk/openjdk8)

*	[adoptopenjdk/openjdk8:alpine-slim](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/alpine/Dockerfile.hotspot.releases.slim)
*	[adoptopenjdk/openjdk8:slim](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/ubuntu/Dockerfile.hotspot.releases.slim)

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-kafka:latest" .
~~~
### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-kafka" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run by Docker-Compose

See [docker-compose.yml](docker-compose.yml).

Use the `ROLE` environment variable or the `entrypoint-<role>.sh` script to start the Kafka/Zookeeper node in a particular role.
The role can be one of the following ('kafka' is the default role, that is, if the `ROLE` variable is not set):

*	`kafka` -- for a Kafka node
*	`zookeeper` -- for a Zookeeper node
